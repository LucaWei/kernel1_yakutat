#include "c3d.hpp"

//===================c3d_func.cpp=====================
extern array_int_type KSYM;
extern value_type FREQ;
//====================================================


extern array_value_type URA, VRA, RCA;
extern array_value_type URB, VRB, RCB;
extern value_type DUA, DVA;
extern value_type DUB, DVB;


//                                     SUM1 = SUM(COS(2*PI*K*F)/K )
//  CALCULATE FREQUENCY SUMMATION TERM SUM2 = SUM(SIN(2*PI*K*F)/K2)
//                                     SUM3 = SUM(COS(2*PI*K*F)/K3)
//                                     SUM4 = SUM(SIN(2*PI*K*F)/K4)
//     FREQUENCY RANGE : F=(-1,1),  K2=K**2, K3=K**3, K4=K**4
bool series(value_type & FREQ, value_type & SUM1, value_type & SUM2, value_type & SUM3, value_type & SUM4)
{
    value_type S1, X0, U, U2, FAC1, V, V2;

    S1=1;
    X0 = FREQ;
    if(X0 < 0.) ++X0;

    if(X0 <= 0.5) //1
    {
        if(X0 != 0.) //2
        {
            if(X0 < 0.3) //3
            {
                U=PI*X0;
                U2=U*U;
                FAC1=log(2.*sin(U));
                SUM1=-FAC1;
                SUM2=2.*S1*U*(-FAC1+1.
                  -U2*(0.111111111+U2*(0.004444444+U2*(0.000302343
                  +U2*(0.000023516+U2* 0.000001944 )))));
                SUM3=ZETA3+2.*U2*(FAC1-1.5
                  +U2*(0.138888889+U2*(0.005185185+U2*(0.000340136
                  +U2*(0.000025867+U2* 0.000002106 )))));
                SUM4=2.*S1*U*(ZETA3+0.666666667*U2*(FAC1-1.833333333
                  +U2*(0.150000000+U2*(0.005396825+U2*(0.000348534
                  +U2*(0.000026295+U2* 0.000002130 ))))));
                return true;
            }
            else
            {
                V=PI*(0.5-X0);
                V2=V*V;
                FAC1=log(2.*cos(V));
                SUM1=-FAC1;
                SUM2=2.*S1*V*(FAC1
                  +V2*(0.333333333+V2*(0.066666667+V2*(0.019047619
                  +V2*(0.005996473+V2* 0.001988135 )))));
                SUM3=-0.75*ZETA3+2.*V2*(FAC1
                  +V2*(0.416666667+V2*(0.077777778+V2*(0.021428571
                  +V2*(0.006596120+V2* 0.002153813 )))));
                SUM4=2.*S1*V*(0.75*ZETA3-0.666666667*V2*(FAC1
                  +V2*(0.450000000+V2*(0.080952381+V2*(0.021957672
                  +V2*(0.006705147+V2* 0.002179302 ))))));
                return true;
            }
        }
        SUM1=0.;
        SUM2=0.;
        SUM3=ZETA3;
        SUM4=0.;
        return true;
    }

    X0=1.- X0;
    S1=-1.;
    if(X0 != 0.) //2
    {
        if(X0 < 0.3) //3
        {
            U=PI*X0;
            U2=U*U;
            FAC1=log(2.*sin(U));
            SUM1=-FAC1;
            SUM2=2.*S1*U*(-FAC1+1.
                -U2*(0.111111111+U2*(0.004444444+U2*(0.000302343
                +U2*(0.000023516+U2* 0.000001944 )))));
            SUM3=ZETA3+2.*U2*(FAC1-1.5
                +U2*(0.138888889+U2*(0.005185185+U2*(0.000340136
                +U2*(0.000025867+U2* 0.000002106 )))));
            SUM4=2.*S1*U*(ZETA3+0.666666667*U2*(FAC1-1.833333333
                +U2*(0.150000000+U2*(0.005396825+U2*(0.000348534
                +U2*(0.000026295+U2* 0.000002130 ))))));
            return true;
        }
        else
        {
            V=PI*(0.5-X0);
            V2=V*V;
            FAC1=log(2.*cos(V));
            SUM1=-FAC1;
            SUM2=2.*S1*V*(FAC1
                +V2*(0.333333333+V2*(0.066666667+V2*(0.019047619
                +V2*(0.005996473+V2* 0.001988135 )))));
            SUM3=-0.75*ZETA3+2.*V2*(FAC1
                +V2*(0.416666667+V2*(0.077777778+V2*(0.021428571
                +V2*(0.006596120+V2* 0.002153813 )))));
            SUM4=2.*S1*V*(0.75*ZETA3-0.666666667*V2*(FAC1
                +V2*(0.450000000+V2*(0.080952381+V2*(0.021957672
                +V2*(0.006705147+V2* 0.002179302 ))))));
            return true;
        }
    }

    SUM1=0.;
    SUM2=0.;
    SUM3=ZETA3;
    SUM4=0.;
    return true;
}




    // CALCULATE THE KERNEL VALUE (WITH SYMMETRY)
    // KSYM(3): SYMMETRY CODE FOR PLANES X=0, Y=0, & Z=0
    // ( 0: NO SYMMETRY, +1: EVEN SYMMETRY, -1: ODD SYMMETRY )
    // VAL : KERNEL VALUE ON RETURN
bool kernel(value_type & val)
{
    value_type NS1, valt;
    val = 0;
    NS1 = 1;

    for(size_type I1=0; I1<2; ++I1)
    {
        for(size_type I2=0; I2<2; ++I2)
        {
            for(size_type I3=0; I3<2; ++I3)
            {
                green(valt);
                // std::cout << ">>>>>KERNEL debug1 " << valt << std::endl;
                // std::cout << ">>>>>KERNEL debug1 " << KSYM[2] << " "
                //                                    << KSYM[1] << " "
                //                                    << KSYM[0] << " "
                //                                    << std::endl; 
                // fgetc(stdin);
                // system("Puase");

                if (NS1 == -1 ) valt = -valt;
                val += valt;

                if(KSYM[2] == 0)
                {
                    break;
                }
                else
                {
                    URA[2] = -URA[2];
                    VRA[2] = -VRA[2];
                    RCA[2] = -RCA[2];
                    if(KSYM[2] == -1) NS1 *= -1;
                }

                
            } //10
            
            // Go T0 15
            if (KSYM[1] == 0)
            {
               break;
            }
            else
            {

                URA[1] = -URA[1];
                VRA[1] = -VRA[1];
                RCA[1] = -RCA[1];
                if(KSYM[1] == -1) NS1 *= -1;

                // std::cout<< URA[1]<<"  "<<VRA[1]<<"  "<< RCA[1]<<"  "<<NS1<<std::endl;

            }
           

        }//20
        
        //Go T0 25
        if (KSYM[0]==0)
        {
            return true;
        }
        else
        {
            URA[0] = -URA[0];
            VRA[0] = -VRA[0];
            RCA[0] = -RCA[0];
            if(KSYM[0] == -1) NS1 *= -1;
            // std::cout<<"hi~"<<std::endl;
        }
           
    }//30
    
    return true;

}

    // CALCULATE THE AVERAGE GREEN FUNCTION BETWEEN TWO RECTANGULARS A & B
    // INTEGRAL FOR AREA A => GAUSS QUADRATURE FORMULA
    // INTEGRAL FOR AREA B => INTEGRATION FORMULA
bool green(value_type & val)
{
    matrix_value_type PS(6, array_value_type(6)), WS(6, array_value_type(6));
    array_value_type DRC(3), DR(3);
    value_type D0, DR2, DDA2, DVA2, DUA2, DUB2, DVB2, DDB2;
    value_type FQU, FQV, FU, WU, FV, WV;
    value_type U0, V0, R0;
    size_type NQU, NQV;
    PS = {
    {.0, .0, .0, .0, .0, .0},
    {-.5773502692, .5773502692, .0, .0, .0, .0},
    {-.7745966692, .0, .7745966692, .0, .0, .0},
    {-.8611363116,-.3399810436, .3399810436, .8611363116, .0, .0},
    {-.9061798459,-.5384693101, .0, .5384693101, .9061798459, .0},
    {-.9324695142,-.6612093865,-.2386191861, .2386191861, .6612093865, .9324695142}
    };

    WS = {
    {2., .0, .0, .0, .0, .0},
    {1., 1., .0, .0, .0, .0},
    {.5555555556, .8888888889, .5555555556, 0., 0., 0.},
    {.3478548451, .6521451549, .6521451549, .3478548451, .0, .0},
    {.2369268851, .4786286705, .5688888889, .4786286705, .2369268851, .0,},
    {.1713244924, .3607615730, .4679139346, .4679139346, .3607615730, .1713244924}
    };

    // FIND RELATIVE POSITION VECTOR
    for(size_type ix=0; ix<3; ++ix)
    {
        DRC[ix] = RCB[ix] - RCA[ix];
    }

    DR2 = DRC[0]*DRC[0] + DRC[1]*DRC[1] + DRC[2]*DRC[2];
    DUA2 = DUA*DUA;
    DVA2 = DVA*DVA;
    DDA2 = DUA2+DVA2;
    // FIND MUTUAL CAPACITANCE
    if(DR2 > 0)
    {
        DUB2=DUB*DUB;
        DVB2=DVB*DVB;
        DDB2=DUB2+DVB2;
        if(DDA2 >= DDB2) 
        {
            FQU=DUB2/DR2;
            FQV=DVB2/DR2;
        }
        else
        {
            FQU=DUA2/DR2;
            FQV=DVA2/DR2;
        }
        

        NQU=1;
        NQV=1;

        if(FQU >= 0.05) NQU = 2;
        if(FQU >= 0.25) NQU = 3;
        if(FQU >= 1.  ) NQU = 4;
        if(FQV >= 0.05) NQV = 2;
        if(FQV >= 0.25) NQV = 3;
        if(FQV >= 1.  ) NQV = 4;
        // USING FOUR-POINT GAUSS QUADRATURE FORMULA
        val = 0.0;

        if(DDA2 >= DDB2)
        {

            for(size_type IU=0; IU<NQU; ++IU)
            {
                //  std::cout<<"hi~"<<std::endl;
                FU=DUB*PS[NQU-1][IU]/2.;
                WU=WS[NQU-1][IU];
                for(size_type IV=0; IV<NQV; ++IV)
                {
                    FV=DVB*PS[NQV-1][IV]/2.;
                    WV=WS[NQV-1][IV];

                    for(size_type IX=0; IX<3; ++IX)
                        DR[IX] = DRC[IX]+URB[IX]*FU+VRB[IX]*FV;

                    U0 = DR[0]*URA[0]+DR[1]*URA[1]+DR[2]*URA[2];
                    V0 = DR[0]*VRA[0]+DR[1]*VRA[1]+DR[2]*VRA[2];
                    R0 = std::sqrt(DR[0]*DR[0]+DR[1]*DR[1]+DR[2]*DR[2]);
                    val += averg(U0,V0,R0,DUA,DVA)*WU*WV;
                    // std::cout<<WU<<"  "<<WV<<std::endl;
                }
            }
            val /= 4.;
            return true;
        }

        for(size_type IU=0; IU<NQU; ++IU)
        {
            FU=DUA*PS[NQU-1][IU]/2.;
            WU=WS[NQU-1][IU];
            for(size_type IV=0; IV<NQV; ++IV)
            {
                FV=DVA*PS[NQV-1][IV]/2.;
                WV=WS[NQV-1][IV];

                for(size_type IX=0; IX<3; ++IX)
                    DR[IX] = DRC[IX]+URA[IX]*FU+VRA[IX]*FV;

                U0 = DR[0]*URB[0]+DR[1]*URB[1]+DR[2]*URB[2];
                V0 = DR[0]*VRB[0]+DR[1]*VRB[1]+DR[2]*VRB[2];
                R0 = std::sqrt(DR[0]*DR[0]+DR[1]*DR[1]+DR[2]*DR[2]);
                val += averg(U0,V0,R0,DUB,DVB)*WU*WV;
            }
        }
        val /= 4.;
        return true;
    }





    // FIND SELF CAPACITANCE
    D0 = std::sqrt(DDA2);
    val = ( std::log((D0+DUA)/DVA)/DUA + std::log((D0+DVA)/DUA)/DVA
          -((D0-DUA)/DVA2+(D0-DVA)/DUA2)/3.0)*2.0;
    return true;
}





    // AVERAGE OF GREEN FRNCTION
    // (XC,YC) : COORDINATE OF CENTER POINT
    // R0 : DISTANCE BETWEEN ORIGIN AND CENTER POINT
    // DX, DY : INTEGRATION LENGTH IN X- AND Y-DIRECTIONS
value_type averg(value_type XC, value_type YC, value_type R0, value_type DX, value_type DY)
{
    value_type AVEGR;
    value_type X02, Y02, R2, XY2, DX2, DY2, A2;
    value_type B4, C4, D4, F2, F0;
    value_type X0, Y0, Z02, Z0, DXH, DYH, XP, XM, YP, YM, XP2, XM2, YP2, YM2;
    value_type RPP, RPM, RMP, RMM;
    value_type ZPP, ZPM, ZMP, ZMM, ZP, ZM;
    value_type ANG, T1, T2;


    X02 = XC * XC;
    Y02 = YC * YC;
    R2  = R0 * R0;
    XY2 = X02 + Y02;
    DX2 = DX * DX;
    DY2 = DY * DY;
    A2  = DX2 + DY2;


    if(A2 < 0.5*R2)
    {
        // USING APPROXIMATE INTEGRATION FORMULA (ERROR < 0.01%)
        if(A2 <= 0.01*R2)
        {
            // USING POINT GREEN FUNCTION
            AVEGR=(1.-A2/(24.*R2))/R0;
            return AVEGR;
        }

        B4=X02*DX2+Y02*DY2;
        C4=DX2*DY2;
        D4=X02*Y02;
        F2=1./R2;
        F0=1./R0;
        AVEGR=F0*(1.-F2*(A2/24.
                    -F2*((240.*B4+9.*A2*A2-8.*C4)/1920.
                    -F2*((9.*B4*A2-4.*XY2*C4)/192.
                    -F2*(21.*B4*B4+28.*C4*D4)/384.     ))));
        return AVEGR;
    }

    X0 =std::abs(XC);
    Y0 =std::abs(YC);
    Z02=std::abs(R2-XY2);
    Z0 =sqrt(Z02);
    DXH=DX/2.;
    DYH=DY/2.;
    XP=X0+DXH;
    XM=X0-DXH;
    YP=Y0+DYH;
    YM=Y0-DYH;
    XP2=XP*XP;
    XM2=XM*XM;
    YP2=YP*YP;
    YM2=YM*YM;
    RPP=sqrt(XP2+YP2+Z02);
    RPM=sqrt(XP2+YM2+Z02);
    RMP=sqrt(XM2+YP2+Z02);
    RMM=sqrt(XM2+YM2+Z02);
    if(Z0 <= 1.0e-06)
    {
        AVEGR=(YP*log((XP+RPP)/(XM+RMP))+YM*log((XM+RMM)/(XP+RPM))
            +XP*log((YP+RPP)/(YM+RPM))+XM*log((YM+RMM)/(YP+RMP))
            +Z0*ANG);
        AVEGR=AVEGR/(DX*DY);
        return AVEGR;
    }

    ZPP=Z02+YP2+YP*RPP;
    ZPM=Z02+YM2+YM*RPM;
    ZMP=Z02+YP2+YP*RMP;
    ZMM=Z02+YM2+YM*RMM;
    ZP=XP*Z0;
    ZM=XM*Z0;
    // BE CAREFUL IN FINDING THE ARC-TANGENT VALUE
    if(XM < 0. || YM < 0.)
    {
        ANG=atan2(ZP,ZPP)-atan2(ZM,ZMP)+atan2(ZM,ZMM)-atan2(ZP,ZPM);
        AVEGR=(YP*log((XP+RPP)/(XM+RMP))+YM*log((XM+RMM)/(XP+RPM))
            +XP*log((YP+RPP)/(YM+RPM))+XM*log((YM+RMM)/(YP+RMP))
            +Z0*ANG);
        AVEGR=AVEGR/(DX*DY);
        return AVEGR;
    }

    T1=ZP*(ZPM-ZPP)/(ZPM*ZPP+ZP*ZP);
    T2=ZM*(ZMP-ZMM)/(ZMP*ZMM+ZM*ZM);
    ANG=atan((T1+T2)/(1.-T1*T2));
    AVEGR=(YP*log((XP+RPP)/(XM+RMP))+YM*log((XM+RMM)/(XP+RPM))
        +XP*log((YP+RPP)/(YM+RPM))+XM*log((YM+RMM)/(YP+RMP))
        +Z0*ANG);
    AVEGR=AVEGR/(DX*DY);
    return AVEGR;
}


//......................................................................
//
//      F A S T   F O U R I E R   T R A N S F O R M
//   VARIABLES:
//     X(N) : COMPLEX SIGNAL
//     M : LEVEL, N=2**M
//     KIND : 1(D) FOR FFT, -1 FOR IFFT
//......................................................................
bool FFT(array_complex_type & X, size_type M, int KIND)
{
    complex_type T, U, W;
    size_type N, N2, N1, J, K, LE, LE1, IP, II;
    N = pow(2,M);
    // std::cout << ">>>>> FFT debug0 " << N << std::endl;
    
    if(KIND!=-1)
    {
        N1=N-1;
        N2=N/2;
        J=0;
        for(size_type I=1;I<N1; ++I)
        {
            K=N2;
            while(!(K >= J))
            {
                J=J-K;
                K=K/2;
            }

            J=J+K;
            if(!(I > J))
            {
                T=X[J];
                X[J]=X[I];
                X[I]=T;
            } 
            
        }
        // DECIMATION IN TIME
        for(size_type L=1;L<M+1; ++L)
        {
            LE = pow(2, L);
            LE1=LE/2;
            U=1.0;
            W.imag(-PI/value_type(LE1));
            W.real(0.0);
            W = std::exp(W);
            for(size_type J=0;J<LE1; ++J)
            {
                for(size_type I=J;J<N; I+=LE)
                {

                    IP = I + LE1;
                    T = X[IP]*U;
                    X[IP] = X[I] - T;
                    X[I] += T;
                }
                U *= W;
            }
        }
        return true;
    }





    
    for(size_type I=0; I<N; ++I)
    {
        X[I] = conj(X[I]);
    }
    N1=N-1;
    N2=N/2;
    J=1;
    
    for(size_type I=1;I<N1; ++I)
    {
        K=N2;
        while(!(K >= J))
        {
            J=J-K;
            K=K/2;
        }

        J=J+K;
        II = I+1;
        
        if(!(II > J))
        {
            // std::cout << ">>>>> FFT debug00 " << I << "  " << J << " " << K << std::endl;
            T=X[J-1];
            X[J-1]=X[I];
            X[I]=T;
        } 
    }
    // std::cout << ">>>>> FFT debug000 " << X[2] << std::endl;
    
    // DECIMATION IN TIME
    for(size_type L=1;L<M+1; ++L)
    {
        LE = pow(2, L);
        LE1=LE/2;
        U=1.0;
        W.imag(-PI/value_type(LE1));
        W.real(0.0);
        W = std::exp(W);
        // std::cout << ">>>>> FFT debug1 " << L << "  " << W << " " << X[1] << std::endl;

        for(size_type J=0;J<LE1; ++J)
        {
            
            for(size_type I=J;I<N; I+=LE)
            {
                // std::cout << J << "  " << I << "  " << N << std::endl;
                IP = I + LE1;
                T = X[IP]*U;
                X[IP] = X[I] - T;
                X[I] += T;
            }
            U *= W;
        }
    }
    // std::cout << ">>>>> FFT debug2 " << X[1] << std::endl;
    for(size_type I=0; I<N; ++I)
    {
        X[I] = conj(X[I]/value_type(N));
    }
    return true;
}


bool gels(array_value_type & R, array_value_type & A, 
size_type M, size_type N, value_type EPS, int IER, array_value_type & AUX)
{
    value_type PIV, TPIV, TOL, TB, PIVI;
    size_type L, IMAX, JMAX, LST, NM, LEND, LT, LL, LR, LLST, II, K, L_, k_;
    std::ofstream debug_file("debug.dat", std::ofstream::app);
    debug_file << "========================================================" <<std::endl;
    if(M <= 0)
    {
        IER = -1;
        return false;
    }
    // SEARCH FOR GREATEST MAIN DIAGONAL ELEMENT
    IER = 0;
    PIV = 0;
    L = 0;
    for(size_type k=0; k<M; ++k) // 30
    {
        L += k+1;
        L_ = L - 1;
        TPIV = std::abs(A[L_]);
        if(!(TPIV <= PIV))
        {
            k_ = k+1;
            PIV  = TPIV;
            IMAX = L_;
            JMAX = k;
        }
    }
    IMAX++; // equal to Fortran
    JMAX++; // equal to Fortran

    TOL=EPS*PIV;
    
    // <-- MAIN DIAGONAL ELEMENT A(IMAX)=A(JMAX,JMAX) IS FIRST PIVOT ELEMENT,
    //     PIV CONTAINS THE ABSOLUTE VALUE OF A(IMAX)
    //
    // <-- START ELIMINATION
    LST = 0;
    NM = N * M;
    LEND = M-1;













    for(size_type k=0; k<M; ++k) // 150
// <-- TEST ON USEFULNESS OF SYMMETRIC ALGORITHM
    {
        
        if(!(PIV > 0.))
        {
            IER = -1;
            std::cout << "PIVOT.EQ.0 ERROR AT K= " << k << std::endl;
            return false;
        }
        
        if(IER == 0 && PIV <= TOL) IER=k-1;
        LT = JMAX-k-1; // equal to Fortran
        LST= LST+k+1; // equal to Fortran
// <-- PIVOT ROW REDUCTION AND ROW INTERCHANGE IN RIGHT HAND SIDE R

        PIVI = 1.0/A[IMAX-1];
        // debug_file<< k << "  " << std::setprecision(17)<< LT
        // << "  " << LST << "  " << IMAX  << std::endl;

        for(L=k; L<NM; L+=M) // 60
        {
            LL = L + LT; 

            

            TB = R[LL] * PIVI;
            R[LL]=R[L];
            R[L]=TB;

            debug_file<< k << "  " << std::setprecision(17)<< L
            << "  " << LL << "  " << TB  << std::endl;
        }
        // std::cout << ">>>>>>>> GELS 1  " << k << " " << R[2] << std::endl;
        
// <-- IS ELIMINATION TERMINATED?
        if(k >= M-1)
        {
            
            if(LEND == 0) return true;
            if(LEND>0)
            {
                break;
            }
            IER = -1;
            return false;
        }
// <-- ROW AND COLUMN INTERCHANGE AND PIVOT ROW REDUCTION IN MATRIX A
//     ELEMENTS OF PIVOT COLUMN ARE SAVED IN AUXILIARY VECTOR AUX
        LR = LST+(LT*(k+JMAX)/2);
        LL = LR;
        L  = LST;
        for(size_type ii=k; ii<LEND; ++ii) // 100
        {
            L=L+ii+1;
            LL=LL+1;
            
            if(L != LR)
            {
                if(L > LR) LL=L+LT;
                TB=A[LL-1];
                A[LL-1]=A[L-1];
            }
            else
            {
                A[LL-1]=A[LST-1];
                TB=A[L-1];
            }
            

            AUX[ii]=TB;
            
            A[L-1]=TB*PIVI;
            
           
        }
        
// <-- SAVE COLUMN INTERCHANGE INFORMATION
        A[LST-1] = double(LT);
        // debug_file<< k << "  " << "  "<< LST-1 << "  " <<std::setprecision(17) << A[LST-1]<< std::endl;
// <-- ELEMENT REDUCTION AND SEARCH FOR NEXT PIVOT
        PIV=0.;
        LLST=LST;
        LT=-1;

        

        for(size_type ii=k; ii<LEND; ++ii) // 140
        {
            
            PIVI=-AUX[ii];
          
            LL=LLST; // LL need -1
            LT=LT+1;
        
            for(size_type lld=ii; lld<LEND; ++lld) // 110
            {
                LL=LL+lld+1;
                L=LL+LT+1;
                A[L-1]=A[L-1]+PIVI*A[LL-1];
                
            }
            
            
            LLST=LLST+ii+1;
            LR=LLST+LT+1;   // LR equal to Fortran
            TPIV=std::abs(A[LR-1]); 
            
            
            if(TPIV <= PIV) 
            {
                
                for(LR=k; LR<NM; LR+=M) // 130
                {
                    LL=LR+LT+1;
                    R[LL]=R[LL]+PIVI*R[LR];

                   
                }
                
            }
            else
            {
                PIV=TPIV;
                IMAX=LR;
                JMAX=ii+2;
                // debug_file<< k << "  " << "  "<< ii << "  "<< JMAX << std::endl;
                for(LR=k; LR<NM; LR+=M) // 130
                {
                    LL=LR+LT+1;
                    R[LL]=R[LL]+PIVI*R[LR];

                
                }
                
                
            }
            
            
        }
  
    }
    
            
    
// <-- END OF ELIMINATION LOOP
//
// <-- BACK SUBSTITUTION AND BACK INTERCHANGE

    
    // debug_file<< LEND << "  " <<std::setprecision(17) << A[20]<< std::endl;
    
    
    II = M;
    for(size_type i=1; i<M; ++i) // 280
    {
        LST=LST-II; // equal to Frotran
        II=II-1;    // equal to Frotran
        L=A[LST-1]+0.5;
        for(size_type j=II-1; j<NM; j+=M) // 270
        {
            
            TB=R[j];
            LL=j;
            K=LST-1;
            for(LT=II; LT<LEND; ++LT) // 260
            {
                LL=LL+1;
                K=K+LT;
                TB=TB-A[K]*R[LL];
            }
            K=j+L;
            R[j]=R[K];
            R[K]=TB;
        }
    }
    return true;
}



bool cgels(array_value_type & R, array_value_type & A, 
size_type M, size_type N, value_type EPS, int IER, array_value_type & AUX)
{

    complex_type TPIV, PIVI, TB;
    array_complex_type R_(R.size()), A_(A.size()), AUX_(AUX.size());
    size_type L{0}, IMAX{0}, JMAX{0}, LST{0}, NM{0}, LEND{0}, LT{0}, LL{0};
    value_type PIV, TOL;

    for(size_type i=0; i<R.size(); ++i)
        R_[i].real(R[i]);
    for(size_type i=0; i<A.size(); ++i)
        A_[i].real(A[i]);
    for(size_type i=0; i<AUX.size(); ++i)
        AUX_[i].real(AUX[i]);

    std::cout << "1CGELS >>>debug " << A_[0] << std::endl; 
    if(!(M>0))
    {
        IER = -1;
        return false;
    }
// <-- SEARCH FOR GREATEST MAIN DIAGONAL ELEMENT
    IER = 0;
    PIV = 0;
    for(size_type k=0; k<M; ++k) // 30
    {
        L += k+1;
        std::cout << "1.5CGELS >>>debug " <<k << " " << L-1 << " " <<
        A_[L-1]
        << std::endl;
        TPIV = std::abs(A_[L-1]);
        if(!(TPIV.real()<=PIV))
        {
            PIV=TPIV.real();
            IMAX=L;
            JMAX=k;
        }
        // std::cout << "2CGELS >>>debug " << k << " " <<
        // IMAX << " " <<
        // JMAX << std::endl;
    }
    TOL = EPS * PIV;
// <-- MAIN DIAGONAL ELEMENT A(IMAX)=A(JMAX,JMAX) IS FIRST PIVOT ELEMENT,
//     PIV CONTAINS THE ABSOLUTE VALUE OF A(IMAX)
//
// <-- START ELIMINATION
    LST = 0;
    NM = M*N;
    LEND = M-1;

    for(size_type k=0; k<M; ++k) // 150
// <-- TEST ON USEFULNESS OF SYMMETRIC ALGORITHM
    {
        if(!(PIV > 0.))
        {
            IER = -1;
            std::cout << "PIVOT.EQ.0 ERROR AT K= " << k << std::endl;
            return false;
        }
        if(IER==0 && PIV<=TOL) IER = k;
        LT = JMAX - k;
        LST += k;
// <-- PIVOT ROW REDUCTION AND ROW INTERCHANGE IN RIGHT HAND SIDE R
        PIVI = 1.0 / A_[IMAX];
        for(size_type L=k; L<NM; L+=M) // 60
        {
            LL = L + LT;
            TB = R_[LL] * PIVI;
            R_[LL] = R_[L];
            R_[L] = TB;
        }
// <-- IS ELIMINATION TERMINATED?
    }
    return true;
}