#include "c3d.hpp"

extern yakutat::DynamicMatrix<value_type> lhs_mat;
extern array_value_type x, rhs; 

/*---------------------------------------------------*/
/* DIVISION-GENERATING PROGRAM                  　　 */
/*       CODE=0 : UNIFORM DIVISION              　　 */
/*            11 : 0.1, UNIFORM DIVISION, 0.1   　　 */
/*            12 : 0.1, UNIFORM                 　　 */
/*            13 : UNIFORM 0.1                  　　 */
/*            21 : 1-�X�, X LIES IN (-1,1)     　　*/
/*            22 : 1-�X�, X LIES IN (-1,0)     　　*/
/*            23 : 1-�X�, X LIES IN (0,-1)     　　*/
/*            24 : 1/(1-�X�), X LIES IN (-1,1) 　　*/
/*---------------------------------------------------*/

// SUBROUTINE DIVGEN(NU,NC,DIV)
void  divgen(size_type nu, size_type nc, array_value_type& div)
{
    size_type i, i0, i1;
    value_type vmx, vi, vf, dv, vv;

    if ( nc >= 11 && nc <= 13 )
    {
        //GO TO 10
        div[0] = 0.1;
        div[nu-1] = 0.1;
        i0 = 1;
        i1 = nu-2;

        if ( nc == 12 ) { i1 = nu-1 ;}
        if ( nc == 13 ) { i0 = 0 ;}


        if ( i1 < i0 )
        {
           
        }
        else
        {
             for ( i = i0; i <= i1; ++i) // "<=" important!!
                div[i] = 1.0; 
        }
         
    }
    else if (nc >= 21 && nc <= 24)
    {
        //GO TO 20
        vi = -1.0;
        vf = 1.0;

        if ( nc == 22 ) { vf = 0. ;}
        if ( nc == 23 ) { vi = 0. ;}

        dv = (vf - vi)/ (nu+1);

        for (i = 0; i < nu; ++i)
        {
            vv = vi + dv*(i+1);
            // std::cout<<"\n vv = "<<vv<<std::endl;
            div[i] = 1.-std::abs(vv);

            if ( nc == 24 ) { div[i] = 1./div[i];}
        }
    }
    else
    {
        for ( i = 0; i < nu; ++i )
            div[i] = 1.;
    }

    vmx = 0.;
    for (i = 0; i < nu; ++i)
    {
        if ( vmx < div[i])
            vmx = div[i];
        
        // std::cout<<vmx<<std::endl;
    }

    for (i = 0; i < nu; ++i)
        div[i] = div[i]/vmx;

}

/*---------------------------------------------------*/
/*     FUNCTION TO TRANSFORM THE CHARACTER CODE      */
/*      TABLE: F,P,R,L,T,B,X,Y,Z, C, *, &, G  ELSE   */
/*             4,1,5,2,6,3,7,8,9,10,11,12,13    0    */
/*---------------------------------------------------*/

//FUNCTION ICHR(C0)
size_type ichr(const string_type c0)
{
    size_type i;
    array_string_type cc{"P","L","B","F","R","T","X","Y","Z","C","*","&","G"};

    for ( i = 0; i < 13 ; ++i)
    {
        if ( c0 == cc[i] )
        {
            return i+1; // plus one
        }
    }

    return 0;
}


/*-------------------------------------------------------------*/
/*                                                             */
/*    VARIABLE INFORMATION                                     */
/*                                                             */
/*     NSURF: # OF SURFACES IN EACH CONDUCTOR GROUP            */
/*     USURF, VSURF: DIRECTION FOR UNIT VECTOR U, V            */
/*     RSURF: ORIGIN(LEFT-LOWER CORNER) COORDINATE OF SURFACE  */
/*     NDSUR: NUMBER OF DIVISION SEGMENT IN DIRECTION U, V     */
/*     DSURF: LENGTH OF DIVISION SEGMENT (#=NDSUR)             */
/*                                                             */
/*-------------------------------------------------------------*/


extern array_int_type KINDC,NCVAR;
extern size_type         NCOND;
extern int               NTVAR, NREP, NTSUR;
extern value_type        UNIT, PITCH, FREQ;
extern matrix_value_type USURF, VSURF, RSURF;
extern matrix_size_type NDSUR;
extern array_value_type  DSURF;
extern array_int_type KSYM, NSYM;
  
bool input(const size_type& ncond, const array_int_type& ksym, const array_value_type& input_puse_info, array_int_type& nsurf_in, array_value_type& allvop1,
           array_value_type& allvop0, array_value_type& allvop2, array_size_type& allnu, array_size_type& allnv, array_value_type& alludiv_vdiv) 
{
    value_type rdiel, scale, hp;
    bool if_debug = true; 
    const value_type eps = 1.0e-06;

    // INPUT PHASE (NOTE: SCALE IS MEASURED WITH RESPECT TO CENTIMETER)
    NCOND = ncond;
    KSYM[0] = ksym[0]; KSYM[1] = ksym[1]; KSYM[2] = ksym[2];
    rdiel = input_puse_info[0];
    scale = input_puse_info[1];
    hp = input_puse_info[2];
    FREQ = input_puse_info[3];


    if ( NCOND > MAXCON)
    {
        std::cout<< ">> ERROR !!! MAXIMAL # OF CONDUCTORS IS "<< MAXCON<<std::endl;
        return false;
    }


     if (if_debug)
    {
        std::cout<<"\n\n-------- >> input info -------- "<<std::endl;
        // std::cout<<" "<<NCOND<<"  "<<KSYM[0]<<"  "<<KSYM[1]<<"  "<<KSYM[2]
        //         <<"  "<<rdiel<<"  "<<scale<<"  "<<hp<<"  "<<FREQ<<std::endl;
        std::cout<< "NCOND = " << NCOND <<"      |  # of conductor groups"<< std::endl <<
                    "NSX = "   << KSYM[0] <<"        |  symmetry with respect to x =0 plane i.e. along x axis"<< std::endl <<
                    "NSY = "   << KSYM[1] <<"        |  symmetry with respect to y =0 plane i.e. along y axis"<< std::endl <<
                    "NSZ = "   << KSYM[2] <<"        |  similar to NSY"<< std::endl <<
                    "RDIEL = "   << rdiel <<"  |  dielectric constant"<< std::endl <<
                    "SCALE = "   << scale <<"  |  unit of the geometry in mm"<< std::endl <<
                    "HP = "   << hp <<"     |  half-pitch for x-direction periodical structure, specify it 0. for non-periodical case"<< std::endl <<
                    "FREQ = "   << FREQ << "   |  useful for spectral analysis(periodical only), specify it 0. for non-periodical case"
                    <<std::endl;
        std::cout<<"------------------ "<<std::endl;
        
    }

    // CALCULATE CONSTANTS
    PITCH = 2.*hp;

    //===============================================================//
    //  NREP IS CODED FROM FREQ, : 0<= FREQ < 1 MEANS ONLY ONE FREQ. //
    //                         FREQ >= 1 MEANS SPECTRAL CALCULATION. //
    //                          FREQ = -1 MEANS X=HP IS METAL PLANE. //
    //===============================================================//
    // std::cout<<"======Debug 1.0===="<<std::endl;
    // std::cout<<FREQ<<std::endl;
    // std::cout<<NREP<<std::endl;
    // std::cout<<KSYM[0]<<" "<<KSYM[1]<<" "<<KSYM[2]<<" "<<std::endl;
    // std::cout<<"==================="<<std::endl;

    if (PITCH > 0.)
    {
        if (std::abs(FREQ+1.)> eps )
        {
            //GO TO 1014
            NREP = FREQ;

            if (KSYM[2] == 0 || (NREP == 0 && (std::abs(FREQ) <= eps || std::abs(FREQ-0.5) <= eps)))
            {
                
            }
            else
            {
                // WRITE(4,1200)
                std::cout<<"\n>> ERROR !!!   FOR PERIODICAL STRUCTURE"
                         <<" INTRODUCE OF FREQUENCY OTHER THAN"
                         <<" ZERO OR ONE-HALF DESTROYS X-AXIS SYMMETRY"
                         <<" PLEASE SPECIFY THE X-AXIS SYMMETRY CODE TO ZERO"
                         <<" AND DEFINE THE WHOLE GEOMETRY FOR X LIES IN -(HALF PITCH) TO +(HALF PITCH)"<<std::endl;
                return false;
            }
        }
        else
        {
            if (KSYM[0] != 0 )
            {
                NREP = 0;
                if (KSYM[0] == 1) { FREQ = 0.5;}
                if (KSYM[0] == -1) { FREQ = 0.;}
                if (KSYM[0] == 2) { NREP = -1;}
            }
            else
            {
                //WRITE(4,1215) --> changed to output at terminal!!
                std::cout<<"\n>> ERROR !!! FOR PERIODICAL STRUCTURE,USING FREQUENCY AS -1 FOR METAL PLANE AT X=(HALF-PITCH) APPLYS ONLY FOR X-SYMMETRICAL STRUCTURES"<<std::endl;
                return false;
            }
        }
        
    }
    else
    {
        NREP = 0;
        FREQ = 0.;
    }

    size_type ix;

    for ( ix = 0; ix < 3; ++ix )
    {
        NSYM[ix] = 1;

        if (KSYM[ix] != 2)
        {

        }
        else
        {
            NSYM[ix] = 2;
            KSYM[ix] = 0;
        }
    }

    // std::cout<<"======Debug 2.0===="<<std::endl;
    // std::cout<<FREQ<<std::endl;
    // std::cout<<NREP<<std::endl;
    // std::cout<<KSYM[0]<<" "<<KSYM[1]<<" "<<KSYM[2]<<" "<<std::endl;
    // std::cout<<"==================="<<std::endl;

    //===============================================//
    //                            CALCULATE THE UNIT //
    //            SCALE IS IN THE UNIT OF CENTIMETER //
    //         RDIEL IS RELATIVE DIELECTRIC CONSTANT //
    //                 PITCH IS IN THE UNIT OF SCALE //
    //===============================================//
    array_int_type nsurf(MAXCON);

    if ( PITCH > 0. &&  NREP == 0 ) 
    {
        //GO TO 1030
        if( KSYM[0] == 0 && NSYM[0] == 1 )  
            UNIT = rdiel/(0.8987404*PITCH);
        else
            UNIT = rdiel/(0.8987404*hp);
    }
    else
    {
        UNIT=rdiel*scale/0.8987404;
    }

    for (size_type i = 0; i < NCOND; ++i)
        nsurf[i] = nsurf_in[i];



     if (if_debug)
     {
          for (size_type i = 0; i < NCOND; ++i)
            std::cout<<"nsurf["<<i<<"] = "<<nsurf[i]<<std::endl;

        std::cout<<"UNIT = "<<std::fixed<< std::setprecision(16)<<UNIT<<std::endl;
     }

    //===============================================//
    //            SURFACE DEFINITION PHASE           //      
    //===============================================//

    int nd, nvar, ns, is;
    bool if_in = true;
    array_int_type ktemp(3);
    size_type ic, nu, nv, nu1, nuv, sum = 0, iter = 0, iter2 = 0, iter3 = 0;
    array_value_type xx1(3), xx2(3), xx0(3),div, dxu(3), dxv(3);
    value_type x0, x1, x2, x3, x4, dru, drv, us, vs;

    NTVAR=0;
    NTSUR=0;
    nd = 0;

    for ( ic = 0; ic < NCOND; ++ic) //1160
    {
        for ( ix = 0; ix < 3; ++ix)
            ktemp[ix] = 0;
        
        nvar = 0;
        ns = nsurf[ic];

        sum = iter;

        for ( is = 0; is < ns; ++is )
        {
            for (size_type i = 0; i < 3; ++i )
            {
                xx1[i] = allvop1[sum + is*3 + i];
                xx0[i] = allvop0[sum + is*3 + i];
                xx2[i] = allvop2[sum + is*3 + i];

                iter = iter +1;
            }

            nu = allnu[iter3];
            nv = allnv[iter3];
            iter3 = iter3 + 1;

            nu1 = nu+1;
            nuv = nu+nv;

            for (size_type i = 0; i < nuv; ++i )
            {
                div.push_back(alludiv_vdiv[iter2]);
                iter2 = iter2+1;
            }
          
            // CHECK IF THIS SURFACE GROUNDED OR LYING ON SYMMETRY PLANE

            for ( ix = 0; ix < 3; ++ix )
            {
                if (KSYM[ix] == 0 && NSYM[ix] == 1 )
                {
                    // GO TO 1070
                }
                else
                {
                    x0 = xx0[ix];
                    x1 = xx1[ix];
                    x2 = xx2[ix];
                    x4 = x1 + x2;
                    x3 = x4 - x0;

                    //X0.GT.EPS .AND. X1.GT.EPS.AND. X2.GT.EPS .AND. X3.GT.EPS

                    if ( x0 > eps && x1 > eps && x2 > eps && x3 > eps )
                    {
                        //  GO TO 1070
                    }
                    else
                    {
                        ktemp[ix] = 1;

                        if ( KSYM[ix] == -1 && x4 <= eps )
                        {
                            //  GO TO 1150
                            if_in = false;
                            std::cout<<" >> warning :: change the status of if_in to false"<<std::endl;
                            break;
                        }
                        else
                        {
                             if_in = true;
                        }
                    }
                }
            } // 1070



            if (if_in)
            {
                // std::cout<<if_in<<" hi~"<<std::endl;
                nvar = nvar + nu*nv;
                NTSUR = NTSUR +1;
                

                if ( NTSUR <= MAXSUR )
                {
                    // FIND UNIT VECTOR U, V
                    for ( ix = 0; ix < 3; ++ix)
                    {
                        dxu[ix] = xx1[ix]-xx0[ix];
                        dxv[ix] = xx2[ix]-xx0[ix];
                    }

                    dru = std::sqrt( dxu[0]*dxu[0]+ dxu[1]*dxu[1]+dxu[2]*dxu[2] );
                    drv = std::sqrt( dxv[0]*dxv[0]+ dxv[1]*dxv[1]+dxv[2]*dxv[2] );

                    for ( ix = 0; ix < 3; ++ix)
                    {
                        USURF[ix][NTSUR-1] = dxu[ix]/dru;
                        VSURF[ix][NTSUR-1] = dxv[ix]/drv;
                        RSURF[ix][NTSUR-1] = xx0[ix];
                    }

                    // FIND DIVISION LENGTH DSURF

                    us = 0.;

                    for ( size_type i = 0; i < nu; ++i )
                        us = us+div[i];
                    
                    for (size_type i = 0; i < nu; ++i )
                        DSURF[i+nd] = div[i]*dru/us;
                    
                    vs = 0.;

                    // std::cout<<nu1<<" "<<nuv<<std::endl;
                    for (size_type i = nu1-1; i < nuv; ++i)
                        vs = vs+div[i];

                    for (size_type i = nu1-1; i < nuv; ++i)
                        DSURF[i+nd] = div[i]*drv/vs;
                    
                    NDSUR[0][NTSUR-1] = nu;
                    NDSUR[1][NTSUR-1] = nv;
                    nd = nd + nuv;
           
                }
                else
                {
                    // std::cout<<"here !!"<<std::endl;
                    std::cout<<">> ERROR !!! : MAXIMAL # OF SURFACES IS "<<MAXSUR<<std::endl;
                    return false;
                }
            }
            
        } //1150 :: for ( is = 0; is < ns; ++is ) ; where ns = nsurf[ic]


        KINDC[ic] = ktemp[0]*100 + ktemp[1]*10 + ktemp[2];
        NCVAR[ic] = nvar;
        NTVAR = NTVAR + nvar;

        if (if_debug) { std::cout<<KINDC[ic]<<" "<<NCVAR[ic]<<" "<<NTVAR<<std::endl;}

    } //1160:: for ( ic = 0; ic < NCOND; ++ic)

    int ntelm;

    ntelm = NTVAR*(NTVAR+1)/2;

     if (if_debug) { std::cout<<ntelm<<std::endl;}

    // ouput file information
    // WRITE(4,1230) NCOND,(NSURF(I),I=1,NCOND)
    // WRITE(4,1240) (KINDC(I),I=1,NCOND)
    // WRITE(4,1250) (NCVAR(I),I=1,NCOND)
    // WRITE(4,1260) NTVAR,NTELM



    return true;

}



/*--------------------------------------------------------------*/
/*                                                              */
/*     C A L C U L A T I O N   &   S T O R A G E   P H A S E    */
/*                                                              */
/**-------------------------------------------------------------*/

extern array_value_type   URA, VRA, RCA;
extern value_type DUA,DVA;
extern array_value_type   URB, VRB, RCB;
extern value_type DUB,DVB;
extern value_type UNIT;


// array_value_type URA(3), VRA(3), RCA(3);
// array_value_type URB(3), VRB(3), RCB(3);
// value_type DUA, DVA;
// value_type DUB, DVB;


bool store(array_value_type& r, size_type& kind, array_value_type& rtemp)
{
    //variables
    int  nsym1, nsym2, nsym3;
    int nsx1, nsy1, nsz1, syz;
    value_type eps, pitch2, sum1, sum2, sum3, sum4;
    value_type err, errm, cpr, cpi, xp, xm, val1, val2, term1, term2, check;
    value_type fx1, fyz, fac1, fac2, fac3, fac4, xc0, pk;
    complex_type cp0, cpt;
    size_type i, ip, ix, iu, iv, ip1, iu1, iv1, iy, iz;
    size_type nd, na, nb, nc, nij, na1, nb1, nc1, nd1;
    array_value_type cost(30), sint(30);
    array_value_type rr(3), rr1(3), dxu(3), dxu1(3), dxv(3), dxv1(3);
    array_value_type vmax(MAXSUR);
    bool if_debug = false;


    /// CALCULATE SOME CONSTANTS IF PERIODICAL STRUCTURE

    for (size_type ix = 0; ix < 3; ++ix )
        if ( NSYM [ix] == 2 ){ KSYM[ix] = 0;}

    nsym1 = NSYM[0];
    nsym2 = NSYM[1];
    nsym3 = NSYM[2];
    std::cout << "nsym1= " << nsym1
        << " nsym2= " << nsym2 
        << " nsym3= " << nsym3 << std::endl;
    kind = 1;

    if ( PITCH <= 0. || NREP != 0) 
    {
        // GO TO 1520
        // std::cout<<"hi~~"<<std::endl;
    }
    else
    {
        eps = 1.0E-06;

        if(std::abs(FREQ-0.5) > eps && std::abs(FREQ) > eps)
            kind = 2;
        
        pitch2 = PITCH * PITCH;
        nsx1 = 1 + KSYM[0];
        nsy1 = 1 + KSYM[1];
        nsz1 = 1 + KSYM[2];
        
        fac1 = nsx1*nsy1*nsz1;
        fac2 = 0.;

        series(FREQ, sum1, sum2, sum3, sum4);

        cp0.imag(2.*3.1415926536*FREQ);
        cp0.real(0.);
        cpt.real(1.);
        cpt.imag(0);

        for ( i = 0; i < 30; ++i )
        {
            cpt = cpt*cp0;
            cost[i] = real(cpt);
            sint[i] = imag(cpt);
        }
    }

    // SET UP COORDINATE OF VARIABLE "a"
    nd = 0;
    for ( ip = 0; ip < NTSUR; ++ip ) // 1810
    {
        // std::cout<<'+'<<"        "<<"COMPUTATION OF"<<"  "<< ip+1<<"-th SURFACE"<<std::endl;


        if ( PITCH <= 0. || NREP != 0)
        {
            //GO TO 1540
        }
        else
        {
            for ( i = 0; i < ip; ++i)
                vmax[i] = 0.;
            
            errm = 0.;
        }

        na = nd + 1;
        nb = nd + NDSUR[0][ip];
        nc = nb + 1;
        nd = nb + NDSUR[1][ip];

        // std::cout<< na <<"  "<<nb<<"  "<<nc<<"  "<<nd<<std::endl;

        for ( ix = 0; ix < 3; ++ix)
        {
            URA[ix] = USURF[ix][ip];
            VRA[ix] = VSURF[ix][ip];
            rr[ix] = RSURF[ix][ip];
        }

        // std::cout<<URA[0]<<"  "<<VRA[0]<<"  "<<rr[0]<<std::endl;
        for ( iu = na -1; iu < nb; ++iu) // 1800
        {
            DUA = DSURF[iu];

            for ( ix = 0; ix < 3; ++ix)
            {
                dxu[ix] = URA[ix]*DUA;
                RCA[ix] = rr[ix]+0.5*dxu[ix];
            }

            //  std::cout<<dxu[0]<<"  "<<RCA[0]<<std::endl;
            for ( iv = nc-1; iv < nd; ++iv) // 1780
            {
                DVA = DSURF[iv];
                
                for ( ix = 0; ix < 3; ++ix )
                {
                    dxv[ix] = 0.5*VRA[ix]*DVA;
                    RCA[ix] = RCA[ix] + dxv[ix];

                    // std::cout<< dxv[ix]<<"  ";
                }

                // std::cout<<std::endl;

                // SET UP COORDINATE OF VARIABLE B
                nd1 = 0;
                nij = 0;
                for ( ip1 = 0; ip1 <= ip; ++ip1) //1760 "<=" important
                {
                    na1 = nd1 + 1;
                    nb1 = nd1 + NDSUR[0][ip1];
                    nc1 = nb1 + 1;
                    nd1 = nb1 + NDSUR[1][ip1];
                
                    // std::cout<< na1<<"  "<<nb1<<"  "<<nc1<<"  "<<nd1<<std::endl;

                    for ( ix = 0; ix < 3; ++ix )
                    {
                        URB[ix] = USURF[ix][ip1];
                        VRB[ix] = VSURF[ix][ip1];
                        rr1[ix] = RSURF[ix][ip1];
                    }

                    for ( iu1 = na1-1; iu1 < nb1; ++iu1 )//1750
                    {
                        DUB = DSURF[iu1];
                        
                        for ( ix = 0; ix < 3; ++ix)
                        {
                            dxu1[ix] = URB[ix]*DUB;
                            RCB[ix] = rr1[ix]+0.5*dxu1[ix];
                        }
                        

                        for ( iv1 = nc1-1; iv1 < nd1; ++iv1 ) //1730
                        {

                            DVB = DSURF[iv1];
                            // std::cout<<DVB<<std::endl;

                            for ( ix = 0; ix < 3; ++ix )
                            {
                                dxv1[ix] = 0.5*VRB[ix]*DVB;
                                RCB[ix] = RCB[ix] + dxv1[ix];
                            }

                            if ( ip1 == ip && (iu1 > iu || (iu1 == iu && iv1 > iv)))
                            {
                                //GO TO 1710
                            }
                            else
                            {
                                /*---------------------------------------------------*/
                                /*                 COMPUTATION PHASE                 */
                                /*---------------------------------------------------*/

                                for ( ix = 0; ix < nsym1; ++ix )
                                {
                                    for ( iy = 0; iy < nsym2; ++iy )
                                    {
                                        for ( iz = 0; iz < nsym3; ++iz )
                                        {
                                            // START OF KERNEL OF CALCULATION
                                            //  if (ip == 0 ){std::cout<<KSYM[2] << " "<< KSYM[1] << " "<< KSYM[0] <<std::endl;}
                                            kernel(cpr); // check!!!!
                                            // if (ip == 0 ){ std::cout<<cpr<<std::endl;}
                                            // fgetc(stdin);
                                            
                                            if ( PITCH <= 0. || NREP != 0 )
                                            {
                                                // GO TO 1640
                                                // std::cout<<"say hello! "<<std::endl;
                                            }
                                            else
                                            {
                                                std::cout<<"hi!!"<<std::endl;
                                                //IN CASE FOR PERIODICAL STRUCTURE AND ONLY ONE FREQ.
                                                cpi = 0.;
                                                if( vmax[ip1] < cpr){ vmax[ip1] = cpr;}
                                                
                                                xp = RCA[0] - RCB[0];
                                                xm = RCA[0] + RCB[0];
                                                fx1 = xp + KSYM[0]*xm;
                                                fyz = nsz1*( (RCA[1]-RCB[1])*(RCA[1]-RCB[1])
                                                            + KSYM[1]*(RCA[1]+RCB[1])*(RCA[1]+RCB[1])
                                                            + nsy1*((RCA[2]-RCB[2])*(RCA[2]-RCB[2]) + KSYM[2]*(RCA[2]+RCB[2])*(RCA[2]+RCB[2])) );
                                                fac3 = -0.5*fyz*nsx1;
                                                fac4 = -1.5*fyz*fx1;

                                                std::cout<<"plz check STOR function : fyz parameter"<<std::endl;
                                                // std::cout<<fyz<<std::endl; 
                                                // std::cout<< xp<<"  "<< xm<<"  "<<fx1<<"  "<<fyz<<"  "<<fac3<<"  "<<fac4<<"  "<<std::endl;
                                                //  fgetc(stdin);

                                                if ( nsy1 != 0 || nsz1 != 0)
                                                {
                                                    //GO TO 1610
                                                }
                                                else
                                                {
                                                    syz = nsy1 * nsz1;
                                                    fac2 = fx1 * syz;
                                                    fac3 = ( xp*xp + KSYM[0]*xm*xm)*syz+fac3;
                                                    fac4 = ( xp*xp*xp + KSYM[0]*xm*xm*xm)*syz+fac4;
                                                }

                                                xc0 = RCA[0];

                                                for ( i = 0; i < 30; ++i)
                                                {
                                                    pk = (i+1)*PITCH;
                                                    RCA[0] = xc0 + pk;
                                                    kernel(val1);
                                                    RCA[0] = xc0 - pk;
                                                    kernel(val2);
                                                    term1 = val1 - (fac1 - (fac2-(fac3 - fac4/pk )/pk )/pk )/pk;
                                                    term2 = val2 - (fac1 + (fac2+(fac3 + fac4/pk )/pk )/pk )/pk;

                                                    cpr = cpr +(term1+term2)*cost[i];
                                                    if ( kind == 2 ) {cpi = cpi +(term1-term2)*sint[i];}
                                                    
                                                    check = std::abs(term1)+std::abs(term2) - 0.001*vmax[ip1];

                                                    if ( i >= 2 && check <= 0. )
                                                    {
                                                        //GO TO 1630
                                                        break;
                                                    }
                                                    
                                                }

                                                if ( check > 0. )
                                                {
                                                    err = (check/vmax[ip1]+0.0001)*100;
                                                    if (errm < err) {errm = err;}
                                                }


                                                RCA[0] = xc0;                                
                                                cpr = cpr + 2.*(sum1*fac1+sum3*fac3/pitch2)/PITCH;

                                                if ( kind == 1 )
                                                {
                                                    // GO TO 1640
                                                }
                                                else
                                                {
                                                    cpi = cpi - 2.*(sum2*fac2+sum4*fac4/pitch2)/pitch2;
                                    
                                                    r[nij] = cpr;
                                                    nij = nij+1;
                                                    r[nij] = cpi;
                                                    nij = nij+1;

                                                    // std::cout<<cpr<<"  "<<cpi<<std::endl;
                                                    //GO TO 1650
                                                }
        
                                            }


                                            // 1640 here!!
                                            if ( kind == 1 ||(PITCH <= 0. || NREP != 0))
                                            {
                                                
                                                r[nij] = cpr;
                                                nij = nij+1;
                                                // if (ip == 0 ){std::cout<<cpr;}
                                            }

                                            // if (ip == 0 ){std::cout<<std::endl;}

                                            // END OF KERNEL OF CALCULATION
                                            //1650
                                            if ( NSYM[2] == 1 )
                                            {
                                                //GO TO 1670
                                                break;
                                            }
                                            else
                                            {
                                                URB[2] = -URB[2];
                                                VRB[2] = -VRB[2];
                                                RCB[2] = -RCB[2];
                                            }

                                        }//1660

                                        // 1670
                                        if ( NSYM[1] == 1 )
                                        {
                                            //GO TO 1690
                                            break;
                                        }
                                        else
                                        {
                                            URB[1] = -URB[1];
                                            VRB[1] = -VRB[1];
                                            RCB[1] = -RCB[1];
                                        }
                                    }//1680

                                    //1690
                                    if ( NSYM[0] == 1 )
                                    {
                                        //GO TO 1710
                                        break;
                                    }
                                    else
                                    {
                                        URB[0] = -URB[0];
                                        VRB[0] = -VRB[0];
                                        RCB[0] = -RCB[0];
                                    }


                                }//1700


                            }

                            // END OF COMPUTATION PHASE
                            // 1710

                            for ( ix = 0; ix < 3; ++ix )
                                RCB[ix] = RCB[ix] + dxv1[ix];

                            // if (ip == 1) {std::cout<<RCB[0]<<"  "<<RCB[1]<<"  "<<RCB[2]<<std::endl;}

                        } //1730

                        for ( ix = 0; ix < 3; ++ix )
                            rr1[ix] = rr1[ix] + dxu1[ix];
                        

                        // if (ip == 1) {std::cout<<rr1[0]<<"  "<<rr1[1]<<"  "<<rr1[2]<<std::endl;}

                    } //1750

                } // 1760

                // write(9) (R(i),i=1,Nij) // important!!!
                for ( ix = 0; ix < nij; ++ix)
                    rtemp.push_back(r[ix]);

                // END OF VARIABLE B
                for ( ix = 0; ix < 3; ++ix )
                    RCA[ix] = RCA[ix] + dxv[ix];

                if ( if_debug )
                {
                    if(ip == 0) std::cout<<r[0]<<"   "<<r[1]<<"  "<<nij;
                    if(ip == 0) std::cout<<std::endl;
                    // check kernel !!
                }


            } // 1780

            for ( ix = 0; ix < 3; ++ix )
                rr[ix] = rr[ix] + dxu[ix];

        } //1800

        // IF(ERRM.GT.0.01) WRITE(4,1910) IP,ERRM

    } // 1810 


    return true;
   
   
}

bool poke(array_value_type& a, array_value_type& r, size_type kind, const array_value_type& rtemp)
{

    size_type i, j, ix, iy, iz, i2;
    size_type ij, nij, sum = 0;
    int  nsym1, nsym2, nsym3, nsymt, ns, ntelm;
    value_type sumr, sumi, ttr, tti;
    

    ns = 1;
    ij = 0;
    nsym1 = NSYM[0];
    nsym2 = NSYM[1];
    nsym3 = NSYM[2];
    nsymt = nsym1*nsym2*nsym3;


    // std::cout<<NTVAR<<"  "<<nsym1<<" "<<nsym2<<"  "<<nsym3<<"  "<<nsymt<<std::endl;


    for ( i = 0; i < NTVAR; ++i )
    {
        nij = 0;

        for( j = 0; j <= (i+1)*nsymt*kind; ++j)
        {
            r[j] = rtemp[j+sum];
        }

        sum = sum + (i+1)*nsymt*kind;
           
        
        // if (i ==NTVAR-1){std::cout<<rtemp[i*nsymt*kind+1]<<"  "<<rtemp[i*nsymt*kind-1]<<std::endl;}
        
        
        for ( j = 0; j <= i; ++j )
        {
            sumr = 0.;
            if ( kind == 2 ){ sumi = 0.;}

            for ( ix = 0; ix < nsym1; ++ix)
            {

                for ( iy = 0; iy < nsym2; ++iy)
                {
                    for ( iz = 0; iz < nsym3; ++iz)
                    {
                       
                        
                        if (kind == 1) { ttr = r[nij];}
                        if (kind == 2)
                        {
                            ttr = r[nij];
                            nij = nij +1;
                            tti = r[nij];
                        }

                        nij = nij +1;

                        // if (i ==NTVAR-1){std::cout<<ttr<<std::endl;}
                        // if (i == 2){std::cout<<ttr<<std::endl;}

                        if (ns != 1)
                        {
                            ttr = -ttr;
                            if (kind == 2){tti = -tti;}
                        }

                        //GO TO 3445
                        sumr = sumr + ttr;
                        if(kind == 2) {sumi = sumi + tti;}
                        if(NSYM[2] == 2 && KSYM[2] == -1){ ns = -ns;}
                    } // 3450


                    if(NSYM[1] == 2 && KSYM[1] == -1) {ns = -ns;}

                } //3440

                 if(NSYM[0] == 2 && KSYM[0] == -1){ ns = -ns;}

            }//3430
            
            lhs_mat.set(i,j, sumr);
            a[ij] = sumr;
            // if (i ==  NTVAR-1) std::cout<<sumr<<std::endl;
            if(kind == 2) {a[ij+1] = sumi;}
            ij = ij + kind;

        } // 3420


        if(std::abs(sumr) <= 1.0E-06){ a[ij-kind] = 1.0;}

    } //3410

    // std::cout<<a[10]<<"  "<<a[15]<<"   "<<a[20]<<std::endl;
  // fgetc(stdin);

    if ( NREP == 0 ){return true;}
    
    if (KSYM[0] == 0)
    {
        // TRANSFORM A INTO COMPLEX FORM
        kind = 2;
        ntelm = NTVAR*(NTVAR+1)/2;

        for (size_type i1 = 0; i1 < ntelm; ++i1)
        {
            i = ntelm + 1 - (i1+1);
            i2 = i+i;
            a[i2-1] = 0.;
            a[i2-2] = a[i];
        }

     

    }

    //3500

    
    return true;
}







bool eqcap(array_value_type& a, array_value_type& r, array_value_type& aux, size_type kind)
{
    int NV, KK, KX, NC=0, IER, KK1;
    array_int_type ICGND(MAXCON), KCOND(MAXCON), ICOND(MAXCON);
    size_type NCG{0}, IV{0}, K0{0}, IX{0};
    size_type NI0, NC0, NVI, NT, IELM0, JC0, JC1, JJC0, IJC;
    value_type SUMR, SUMI, DSUM;
    array_string_type NCHAR{"O", "*", "E", "+", "-"};
    matrix_string_type NFLAG(4, array_string_type(MAXCON));
    array_value_type cij(MAXCON*(MAXCON+1)), DCR(MAXCON), DCI(MAXCON), TCR(MAXCON), TCI(MAXCON);
    bool swift{false};

    std::ofstream mat_file;
    mat_file.open("lhs_matrix.dat");
    size_type cnt{0};

    

    for(size_type IC=0; IC<NCOND; ++IC) // 2110
    {
        for(size_type IX=0; IX<3; ++IX) // 2010
        {
            NFLAG[IX+1][NC] = NCHAR[KSYM[IX]+1]; 

            // std::cout << IC << "  " <<
            // IX+1 << "  " <<
            // NC << "  " << 
            // NFLAG[IX+1][NC] << "  " << std::endl;

        } // for end of 2010
        NV = NCVAR[IC]; // 96
        K0 = 1;
        KK = KINDC[IC]; // 0

        // std::cout << IC << "  " << 
        // NCVAR[IC] << "  " << 
        // KINDC[IC] << "  " << std::endl;


        for(size_type I=0; I<3; ++I) // 2050
        {
            IX = 2 - I; 
            KX = KK - KK/(10*10);
           
            if(KX == 0) // go to 2040
            {
                KK/=10; // 2040
            } 
            else
            { 
                NFLAG[IX+1][NC] = NCHAR[1];
                if(IX!=0 || PITCH<=0. || NREP!=0) K0=K0*2;

                if(KSYM[IX]==-1) // go to 2090
                {
                    // 2090
                    ICGND[NCG]=IC;
                    NCG=NCG+1;
                    for(size_type I=0; I<NV; ++I) // 2100
                    {
                        r[IV] = 1.0;
                        if(kind==2) r[IV+1]=0.;
                        IV += kind;
                        swift = true;
                        break; // break for loop 2050
                    }
                }
                else
                {
                    KK/=10;
                }
            } 

        } // for end of 2050

        if(      swift==false     ) // reverse "go to 2090"
        {
            
            KCOND[NC] = K0;
            ICOND[NC] = IC;
            NFLAG[0][NC] = char(48+IC+1);
            if(NC == 0) // go to 2070   // here i change NC == 1 to NC == 0, because NC is index
            {
                //2070
                for(size_type I=0; I<NV; ++I) // 2080
                {
                    
                    r[IV] = 1.0;
                    if(kind == 2) r[IV+1] = 0;
                    IV += kind;

                    
                } // for end of 2080
            }
            else
            {
                for(size_type I=0; I<NTVAR; ++I) // 2060
                {
                    r[IV] = 0.0;
                    if(kind == 2) r[IV+1] = 0.0;
                    IV += kind;
                } // for end of 2060

                //2070
                for(size_type I=0; I<NV; ++I) // 2080
                {
                    r[IV] = 1.0;
                    if(kind == 2) r[IV+1] = 0;
                    IV += kind;
                } // for end of 2080
            }
            NC++;
            
        } 


    } // for end of 2110     index is "IC"

    
    // for(size_type i=0; i<4; ++i)
    // {
    //     for(size_type j=0; j<1; ++j)
    //     {
    //         std::cout << NFLAG[i][j] << std::endl;
    //     }
    // }
    
    // ================ solver Ax = B ================ //
    if(kind == 1)
    {
        for(size_type i=0; i<lhs_mat.row(); ++i)
        {
            for(size_type j=i+1; j<lhs_mat.col(); ++j)
            {
                lhs_mat.set(i,j,lhs_mat(j,i));
            }
        }


        size_type cnt{0};
        for(size_type j=0; j<NC; ++j) // number of conductors
        {
            cnt = NTVAR * j;
            // std::cout << "======== rhs ========" << std::endl;
            for(size_type i=0; i<NTVAR; ++i)
            {
                rhs[i] = r[cnt];
                // std::cout << cnt << "   " << r[cnt] << std::endl;
                ++cnt;
            }
            mat_file << lhs_mat << std::endl;
            
            yakutat::LD< yakutat::DynamicMatrix<value_type> > LD;
            LD.factorize(lhs_mat);
            LD.solve(rhs, x);

            // yakutat::bicgstab< yakutat::DynamicMatrix<value_type> > solver;
            // size_type iters;
            // value_type error;
            // solver.initialize(lhs_mat);
            // solver.setTolerance(1e-10);
            // std::tie(iters, error) = solver.solve(rhs, x);

            cnt = NTVAR * j;
            // std::cout << "======== x ========" << std::endl;
            for(size_type i=0; i<NTVAR; ++i)
            {
                r[cnt] = x[i];
                // std::cout << cnt << "   " << r[cnt] << std::endl;
                ++cnt;
            }
        }
        
    }
    // ================ solver Ax = B ================ //

    if(IER != 0)
        std::cout << "WARNING: LOSS OF SIGNIFICANCE AT ELIMINATION STEP " << IER << std::endl;
//                                FIND THE EQUIVALENT CAPACITANCE MATRIX
    NI0=0;
    NC0=0;
    NVI=0;
    for(size_type IC=0; IC<NCOND; ++IC) // 2160
    {
        NT = NCVAR[IC]; // 10
        if(IC!=ICOND[NC0])
        {
            NVI += NT*kind;
        }
        else
        {
            IELM0=0;

            for(size_type INC=0; INC<NC0+1; ++INC) // 2140
            {
                SUMR = 0.0;
                if(kind==2) SUMI = 0.0;
                IV = IELM0 + NVI;
                for(size_type I=0;I<NT; ++I) // 2130
                {
                    SUMR += r[IV];
                    std::cout << "IV " << I << " " << SUMR << " " << IV  << std::endl;
                    if(kind==2) SUMI += r[IV+1];
                    IV += kind;
                } // 2130
                IELM0 += NTVAR*kind;
                cij[NI0] = SUMR * UNIT;
                if(kind==2) cij[NI0] = SUMI * UNIT;
                NI0 += kind;
            } // 2140
            ++NC0; 
            NVI += NT*kind;
        }
    } //2160
//                                     STORE THE SELF CAPACITANCE VALUE
    DSUM=0.0;
    JC0=0;
    // NC = 0
    for(size_type JC=0; JC<NC; ++JC) // 2200
    {
        DCR[JC] = 0.0;
        if(kind==2) DCI[JC] = 0.0;
        for(size_type IC=0; IC<JC+1; ++IC) // 2170
        {
            DCR[JC] += cij[JC0];
            if(kind==2) DCI[JC] += cij[JC0+1];
            JC0 += kind;
        } // 2170

        if(JC == NC-1)
        {
            DSUM += DCR[JC];
        }
        else
        {
            JC1 = JC + 1;
            JJC0 = JC0 + JC*kind;
            for(size_type IC=JC1; IC<NC; ++IC) // 2180
            {
                DCR[JC] += cij[JJC0];
                if(kind==2) DCI[JC] -= cij[JJC0+1];
                JJC0 += IC*kind;
            } // 2180
        }
    } // 2200








    if(PITCH <= 0.0 || FREQ != 0.0 || KSYM[0] == -1 ||
       KSYM[1] == -1 || KSYM[2] == -1)
//                         CORRECTION FOR INFINITE GREEN'S FUNCTION TERM
    {
        if(NCG>0)
        {
            std::cout << NCG << " CONDUCTORS GROUNDED, THEY ARE : ";
            for(size_type i=0; i<NCG; ++i)
                std::cout << ICGND[i] << " ";
            std::cout << std::endl;
        } 
        for(size_type IC=0; IC<NC; ++IC) // 2225
        {
            KK = KCOND[IC];
            DCR[IC] *= KK;
            if(kind==2) DCI[IC] *= KK;
        } // 2225

        // WRITE(10) NC
        // WRITE(10) (NFLAG(1,I),I=1,NC)
        // WRITE(10) ((NFLAG(K,I),K=2,4),I=1,NC)
        if(kind==1) //WRITE(10) (DCR(I),I=1,NC)

        if(kind==1)
        {
            std::cout << "  CONDUCTORS       :      ";
            for(size_type I=0; I<NC; ++I)
                for(size_type K=0; K<4; ++K)
                {
                    if(K==1)
                        std::cout << "(";
                    std::cout << NFLAG[K][I];
                    if(K==3)
                        std::cout << ")";
                }
                    
            std::cout << std::endl;
        }
//                                        CORRECTION FOR N-FOLD SYMMETRY
        IJC = 0;
        for(size_type IC=0; IC<NC; ++IC) // 2240
        {
            KK = KCOND[IC];
            for(size_type JC=0; JC<IC+1; ++JC) // 2230
            {
                KK1 = KCOND[IC];
                if(KK < KK1) KK1 = KK;
                TCR[JC] = cij[IJC] * value_type(KK1);
                if(kind==2) TCI[JC] = cij[IJC+1] * value_type(KK1);
                IJC += kind;
            }
            if(kind==1)
            {   
                std::cout << DCR[IC] << " : ";
                for(size_type J=0; J<IC+1; ++J)
                    std::cout << TCR[J] << " ";
                std::cout << std::endl;
            }

            if(kind==2)
            {
    //             DO 2231 J=1,IC
    //             TC(J)=cmplx(TCR(J),TCI(J))
    //    2231     CONTINUE
    //             WRITE(4,2370) DCR(IC),DCI(IC),(TC(J),J=1,IC)
    //             WRITE(10) (TC(J),J=1,IC)
            }   

        } // 2240
        return true;
    }
    else
    {
        IJC = 0;
        for(size_type IC=0; IC<NC; ++IC) // 2210
        {
            for(size_type JC=0; JC<IC+1; ++JC) // 2210
            {
                if(kind == 2)
                {
                    cij[IJC]=cij[IJC]-(DCR[IC]*DCR[JC]+DCI[IC]*DCI[JC])/DSUM;
                    cij[IJC+1]=cij[IJC+1]+(DCR[IC]*DCI[JC]-DCI[IC]*DCR[JC])/DSUM;
                    IJC=IJC+2;
                }
                else
                {
                    cij[IJC]=cij[IJC]-DCR[IC]*DCR[JC]/DSUM;
                    ++IJC;
                }
            }
        }
        for(size_type IC=0; IC<NC; ++IC) //2215
        {
            DCR[IC] = 0.0;
            DCI[IC] = 0.0;
        }


        // copy from upper
        if(NCG>0)
        {
            std::cout << NCG << " CONDUCTORS GROUNDED, THEY ARE : ";
            for(size_type i=0; i<NCG; ++i)
                std::cout << ICGND[i] << " ";
            std::cout << std::endl;
        } 
        for(size_type IC=0; IC<NC; ++IC) // 2225
        {
            KK = KCOND[IC];
            DCR[IC] *= KK;
            if(kind==2) DCI[IC] *= KK;
        } // 2225

        // WRITE(10) NC
        // WRITE(10) (NFLAG(1,I),I=1,NC)
        // WRITE(10) ((NFLAG(K,I),K=2,4),I=1,NC)
        if(kind==1) //WRITE(10) (DCR(I),I=1,NC)

        if(kind==1)
        {
            std::cout << "  CONDUCTORS       :      ";
            for(size_type I=0; I<NC; ++I)
                for(size_type K=0; K<4; ++K)
                {
                    if(K==1)
                        std::cout << "(";
                    std::cout << NFLAG[K][I];
                    if(K==3)
                        std::cout << ")";
                }
                    
            std::cout << std::endl;
        }
//                                        CORRECTION FOR N-FOLD SYMMETRY
        IJC = 0;
        for(size_type IC=0; IC<NC; ++IC) // 2240
        {
            KK = KCOND[IC];
            for(size_type JC=0; JC<IC+1; ++JC) // 2230
            {
                KK1 = KCOND[IC];
                if(KK < KK1) KK1 = KK;
                TCR[JC] = cij[IJC] * value_type(KK1);
                if(kind==2) TCI[JC] = cij[IJC+1] * value_type(KK1);
                IJC += kind;
            }
            if(kind==1)
            {   
                std::cout << DCR[IC] << " : ";
                for(size_type J=0; J<IC+1; ++J)
                    std::cout << TCR[J] << " ";
                std::cout << std::endl;
            }

            if(kind==2)
            {
    //             DO 2231 J=1,IC
    //             TC(J)=cmplx(TCR(J),TCI(J))
    //    2231     CONTINUE
    //             WRITE(4,2370) DCR(IC),DCI(IC),(TC(J),J=1,IC)
    //             WRITE(10) (TC(J),J=1,IC)
            }   

        } // 2240
        return true;

    }

    return true;
}
