#include "c3d.hpp"

extern array_int_type    KINDC, NCVAR, NSYM, KSYM;
extern size_type         NCOND;
extern int               NTVAR, NREP;
extern value_type        UNIT, PITCH, FREQ;

extern yakutat::DynamicMatrix<value_type> lhs_mat;
extern array_value_type x, rhs; 

bool calculate()
{
    // variables
    array_value_type a(NTVAR*(NTVAR+1)/2), r(MAXCON*NTVAR), aux(NTVAR);
    array_value_type rtemp;
    size_type kind0, kind;
    int  nsym1, nsym2, nsym3;
    bool status;
    value_type freq0;

    
    x.resize(NTVAR, 0);
    rhs.resize(NTVAR);

    rtemp.reserve(MAXCON*NTVAR);

    // std::cout<<KSYM[0]<<"  "<<KSYM[1]<<"  "<<KSYM[2]<<std::endl;
    // std::cout<<NSYM[0]<<"  "<<NSYM[1]<<"  "<<NSYM[2]<<std::endl;
    // std::cout<<NCOND<<std::endl;
    // std::cout<<KINDC[0]<<"  "<<KINDC[1]<<"  "<<KSYM[2]<<std::endl;
    // std::cout<<NCVAR[0]<<"  "<<NCVAR[1]<<"  "<<NCVAR[2]<<std::endl;
    // std::cout<<UNIT<<"  "<<PITCH<<"  "<<FREQ<<std::endl;

    status = store(r, kind0, rtemp);
    

    if (status)
    {
        for ( size_type ix = 0; ix < 3; ++ix)
        if( NSYM[ix] == 2 ){KSYM[ix] = 1;}

        std::cout<<"\n";
        // std::cout << "KYSM    "<<KSYM[0]<<"  "<<KSYM[1]<<"  "<<KSYM[2] <<"  "<<NSYM[0]
        // <<std::endl;
        

        nsym1 = NSYM[0];
        nsym2 = NSYM[1];
        nsym3 = NSYM[2];
        // open(10,status='SCRATCH',form='UNFORMATTED') ????????
        if ( NREP > 0 && std::abs(FREQ+1.) > 1.0E-06 )
        {
            // NREP > 0, SPRECTRAL CONTRIBUTION
            //GO TO 90 
            // open(11,status='SCRATCH',form='UNFORMATTED')????????

        }
        else
        {
            // NREP=0, NON-PERIODICAL STRUCTURE
            // OR      FREQ = -1, (X = +-HP ARE METAL) & NSYM(1)=2
        
            // for output
            if (PITCH > 0. && NREP == 0) {std::cout<<"* EQUIVALENT CAPACITANCE (PF/CM) MATRIX BETWEEN "<<NCOND<<" CONDUCTORS *   BY R. B. WU"<<std::endl;}
            if (PITCH <= 0.|| NREP != 0) {std::cout<<"* EQUIVALENT CAPACITANCE (PF) MATRIX BETWEEN "<<NCOND<<" CONDUCTORS *   BY R. B. WU"<<std::endl;}
            
            kind = kind0;

            // for output
            if( kind == 1) {std::cout<<"SELF"<<"      "<<":  SHORT-CIRCUIT CAPACITANCE MATRIX"<<std::endl;}
            if( kind == 2) {std::cout<<"SELF"<<"      "<<":  SHORT-CIRCUIT CAPACITANCE MATRIX"<<std::endl;}

            freq0 = FREQ;


            // std::cout << KSYM[0] << " " <<
            // KSYM[1] << " " <<
            // KSYM[2] << std::endl;

            for ( size_type ix = 0; ix < nsym1; ++ix )
            {
                if (std::abs(freq0+1) >= 1.0E-06 ){ FREQ = 0.25*(1+KSYM[0]);}
                
                for ( size_type iy = 0; iy < nsym2; ++iy )
                {
                    for( size_type iz = 0; iz < nsym3; ++iz)
                    {


                        lhs_mat.clear();
                        lhs_mat.resize(NTVAR);
                        poke(a, r, kind, rtemp);
                        


                        eqcap(a, r, aux, kind);
                        std::cout<<"             SOLVING MATRIX EQUATION ..."<<std::endl;


                        if( NSYM[2] == 2) {KSYM[2] = -KSYM[2];} // here                                                                           
                    } //30

                    if(NSYM[1] == 2) {KSYM[1] = -KSYM[1];}

                } // 40

                if (NSYM[0] == 2) {KSYM[0] = -KSYM[0];}

            } // 50
        }


         // 還沒完成
//         IF(NSYM(3).EQ.1) GO TO 60
//         WRITE(4,220)
//         CALL MERGE(A,KIND,3)
//    60 IF(NSYM(2).EQ.1) GO TO 70
//         WRITE(4,230)
//         CALL MERGE(A,KIND,2)
//    70 IF(NSYM(1).EQ.1) GO TO 80
//         WRITE(4,240)
//         CALL MERGE(A,KIND,1)


        return true;

    }
    else
    {
        std::cout<<"ERROR!!!! error in the status of the function store() !!!!"<<std::endl;

        return false;
    }
    
}