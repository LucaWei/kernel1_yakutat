#pragma once

//===================c3d_io.cpp=====================
string_type       infile;
// array_ineger_type nsurf(maxcon), ndiv(3), ncode(3);
// array_ineger_type nsym(3), ncelm(maxcon);           // 重複變數名 !!!!!! NSYM!!!!
// array_value_type  vop1(3), vop2(3), vop0(3), vinp(3), dinp(3), udiv(99), vdiv(99);
// string_type       c0, cc, cf;
// array_string_type ch(6);
//==============================================

//===================c3d_func.cpp=====================
array_int_type KINDC(MAXCON),NCVAR(MAXCON);
size_type NCOND;
int NTVAR, NREP, NTSUR;
value_type  UNIT, PITCH,FREQ;
matrix_value_type USURF(3,array_value_type(MAXSUR)), VSURF(3,array_value_type(MAXSUR)), RSURF(3,array_value_type(MAXSUR));
matrix_size_type NDSUR(2,array_size_type(MAXSUR));
array_value_type DSURF(10*MAXSUR);
array_int_type  KSYM(3), NSYM(3);
//====================================================

//===================c3d_solver.cpp=====================
array_value_type A, R, AUX;
//====================================================



yakutat::DynamicMatrix<value_type> lhs_mat;
array_value_type x, rhs; 



array_value_type URA(3), VRA(3), RCA(3);
array_value_type URB(3), VRB(3), RCB(3);
value_type DUA, DVA;
value_type DUB, DVB;
