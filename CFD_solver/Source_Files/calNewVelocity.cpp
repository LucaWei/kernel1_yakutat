#include <cmath>
#include <omp.h>
#include <iostream>
#include <vector>
#include "MPI_tools.hpp"

#include "Resolution.hpp"

using std::vector;

void calNewVelocity
    (
    // ======================================================== //
    double dt,
    MPI_tools U_MPI,

    vector<double> &P,
    vector<vector<double> > &U_star,
    vector<vector<double> > &NEIBcell,
    vector<vector<double> > &U2,
    vector<vector<double> > &FX,
    vector<double> &ETA,
    
    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs
    // ======================================================== //

    )
{
    // ======================================================== //
    double u_solid = 0.0; 

    double v_solid = 0.0; 

    double w_solid = 0.0; 

    double *U1 = new double[3];
    
    int icel, xp, yp, zp, xm, ym, zm;

    // ======================================================== //

    


    /**************************************************************/
    /*        Calculation of velocity field at t = dt*n+1         */
    /**************************************************************/

    #pragma omp parallel
    {

    //In x direction
    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {   
                icel = i*nz*ny + j*nz + k;

                xm  = NEIBcell[0][icel];
                xp  = NEIBcell[1][icel];

                U1[0] = U_star[0][icel] - dt*( P[xp]-P[xm] ) / (2*iDx[i]);

                U2[0][icel] = ETA[icel] * u_solid + (1-ETA[icel]) * U1[0];

                FX[0][icel] = (U2[0][icel] - U1[0]) / dt;
            }
        }    
    }


    //In y direction
    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                icel = i*nz*ny + j*nz + k;

                ym  = NEIBcell[2][icel];
                yp  = NEIBcell[3][icel];

                U1[1] = U_star[1][icel] - dt*( P[yp]-P[ym] ) / (2*iDy[j]);

                U2[1][icel] = ETA[icel] * v_solid + (1-ETA[icel]) * U1[1];

                FX[1][icel] = (U2[1][icel] - U1[1]) / dt;
            }
        }    
    }

    //In z direction
    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                icel = i*nz*ny + j*nz + k;

                zm  = NEIBcell[4][icel];
                zp  = NEIBcell[5][icel];

                U1[2] = U_star[2][icel] - dt*( P[zp]-P[zm] ) / (2*iDz[k]);

                U2[2][icel] = ETA[icel] * w_solid + (1-ETA[icel]) * U1[2];

                FX[2][icel] = (U2[2][icel] - U1[2]) / dt;
            }
        }    
    }
   
    }

    delete U1;

}

void updating
    (
    // ======================================================== //

    vector<vector<double> > &U,
    vector<vector<double> > &U2,
    MPI_tools U_MPI

    // ======================================================== //

    )
{
    // ======================================================== //
    size_t icel{0};
    
    // ======================================================== //


    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp parallel for simd collapse(2) 
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                icel = i*nz*ny + j*nz + k;

                U[0][icel] = U2[0][icel];
                U[1][icel] = U2[1][icel];
                U[2][icel] = U2[2][icel];
            }
        }    
    }

    
}




























